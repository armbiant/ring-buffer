/**
 * @file
 * @author Martin Stejskal
 * @brief Ring buffer utility for embedded platforms
 */
// ===============================| Includes |================================
#include "ring_buffer.h"

#include <assert.h>
#include <string.h>
// ================================| Defines |================================
#define _GET_NUM_OF_ITEMS(p_ring_buffer) \
  (p_ring_buffer->u16_buffer_size_bytes / p_ring_buffer->u8_item_size_bytes)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Check ring buffer structure
 * @param psRingBuffer Pointer to ring buffer structure
 */
static void _check_ring_buffer(ts_ring_buffer *ps_ring_buffer);

/**
 * @brief Increase index number and check if not overflow. If so, reset index
 * @param pu16idx Pointer to index
 * @param uMaxIdx When reached this value, index will be reset to zero
 */
static void _inc_idx_and_check(uint16_t *pu16_idx, size_t u_max_idx);

/**
 * @brief Decrease index number. If value is lower than 1, then set maximum
 *        index value
 * @param pu16idx Pointer to index
 * @param uMaxIdx If current index is zero, this value will be used
 */
static void _dec_idx_and_check(uint16_t *pu16_idx, size_t u_max_idx);
// =========================| High level functions |==========================
void ring_buffer_push(ts_ring_buffer *ps_ring_buffer, const void *pv_data) {
  // Pointer to data should not be empty
  assert(pv_data);

  _check_ring_buffer(ps_ring_buffer);

  // Calculate correct position in buffer. Need to work at Byte level.
  // First set beginning of buffer and later add offset.
  uint8_t *pu8data = (uint8_t *)ps_ring_buffer->pv_buffer;

  pu8data += ps_ring_buffer->u16_write_idx * ps_ring_buffer->u8_item_size_bytes;

  // Write data to given position
  memcpy(pu8data, pv_data, (size_t)ps_ring_buffer->u8_item_size_bytes);

  _inc_idx_and_check(&ps_ring_buffer->u16_write_idx,
                     _GET_NUM_OF_ITEMS(ps_ring_buffer));
}

void ring_buffer_pull(ts_ring_buffer *ps_ring_buffer, void *pv_data) {
  // Pointer to data should not be empty
  assert(pv_data);

  _check_ring_buffer(ps_ring_buffer);

  // Calculate correct position in buffer. Need to work at Byte level.
  // First set beginning of buffer and later add offset.
  uint8_t *pu8_data = (uint8_t *)ps_ring_buffer->pv_buffer;

  pu8_data += ps_ring_buffer->u16_read_idx * ps_ring_buffer->u8_item_size_bytes;

  // Write data to given position
  memcpy(pv_data, pu8_data, (size_t)ps_ring_buffer->u8_item_size_bytes);

  _inc_idx_and_check(&ps_ring_buffer->u16_read_idx,
                     _GET_NUM_OF_ITEMS(ps_ring_buffer));
}

// ========================| Middle level functions |=========================
void ring_buffer_clean(ts_ring_buffer *ps_ring_buffer) {
  _check_ring_buffer(ps_ring_buffer);

  // Clean content
  memset(ps_ring_buffer->pv_buffer, 0,
         (size_t)ps_ring_buffer->u16_buffer_size_bytes);

  // Reset read & write index
  ps_ring_buffer->u16_read_idx = ps_ring_buffer->u16_write_idx = 0;
}

void ring_buffer_pull_latest(ts_ring_buffer *ps_ring_buffer, void *pv_data) {
  // Pointer to data should not be empty
  assert(pv_data);

  _check_ring_buffer(ps_ring_buffer);

  // Latest value is "behind" write index
  uint16_t u16_idx = ps_ring_buffer->u16_write_idx;
  _dec_idx_and_check(&u16_idx, _GET_NUM_OF_ITEMS(ps_ring_buffer));

  // Calculate correct position in buffer. Need to work at Byte level.
  // First set beginning of buffer and later add offset.
  uint8_t *pu8_data = (uint8_t *)ps_ring_buffer->pv_buffer;

  pu8_data += u16_idx * ps_ring_buffer->u8_item_size_bytes;

  memcpy(pv_data, pu8_data, (size_t)ps_ring_buffer->u8_item_size_bytes);
}

void ring_buffer_pull_relative(ts_ring_buffer *ps_ring_buffer, void *pv_data,
                               const uint16_t u16_relative_idx) {
  // Pointer to data should not be empty
  assert(pv_data);

  uint16_t u16_num_of_items = _GET_NUM_OF_ITEMS(ps_ring_buffer);

  // Relative index should not be bigger than number of items
  assert(u16_relative_idx < u16_num_of_items);

  _check_ring_buffer(ps_ring_buffer);

  // The "latest item" is on "write index" position
  uint16_t u16_idx = ps_ring_buffer->u16_write_idx + u16_relative_idx;

  if (u16_idx >= u16_num_of_items) {
    // Index is bigger than maximum -> need to start from beginning, while
    // deduct "overlap"
    u16_idx = u16_idx - u16_num_of_items;
  }

  // Get pointer for given index
  uint8_t *pu8_data = (uint8_t *)ps_ring_buffer->pv_buffer;

  pu8_data += u16_idx * ps_ring_buffer->u8_item_size_bytes;

  memcpy(pv_data, pu8_data, (size_t)ps_ring_buffer->u8_item_size_bytes);
}
// ==========================| Low level functions |==========================

void ring_buffer_reset_read_idx(ts_ring_buffer *ps_ring_buffer) {
  _check_ring_buffer(ps_ring_buffer);

  ps_ring_buffer->u16_read_idx = ps_ring_buffer->u16_write_idx;
}

inline uint16_t ring_buffer_get_num_of_items(ts_ring_buffer *ps_ring_buffer) {
  _check_ring_buffer(ps_ring_buffer);

  return _GET_NUM_OF_ITEMS(ps_ring_buffer);
}

// ==========================| Internal functions |===========================

static void _check_ring_buffer(ts_ring_buffer *ps_ring_buffer) {
  // Pointer to rung buffer should not be empty
  assert(ps_ring_buffer);
  // Write index should not overlap maximum address
  assert(ps_ring_buffer->u16_write_idx < _GET_NUM_OF_ITEMS(ps_ring_buffer));

  // Items should be aligned to buffer size
  assert((ps_ring_buffer->u16_buffer_size_bytes %
          ps_ring_buffer->u8_item_size_bytes) == 0);
}

static void _inc_idx_and_check(uint16_t *pu16_idx, size_t u_max_idx) {
  *pu16_idx += 1;

  if (*pu16_idx >= u_max_idx) {
    *pu16_idx = 0;
  } else {
    // Keep value as it is
  }
}

static void _dec_idx_and_check(uint16_t *pu16_idx, size_t u_max_idx) {
  if (*pu16_idx >= 1) {
    *pu16_idx -= 1;
  } else {
    // Max index is actually "sizeof(array)" so actually as index we have
    // to use 1 lower
    *pu16_idx = u_max_idx - 1;
  }
}
