/**
 * @file
 * @author Martin Stejskal
 * @brief Test file for ring buffer
 */
// ===============================| Includes |================================
#include "ring_buffer.h"

#include <assert.h>
#include <stdint.h>
#include <stdio.h>

#include "unity.h"
// ================================| Defines |================================
/**
 * @brief Buffer size as number of items
 */
#define _RING_BUFFER_SIZE_ITEMS (5)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================
static uint32_t mau32_buffer_data[_RING_BUFFER_SIZE_ITEMS];
static ts_ring_buffer ms_ring_buffer = {
    .pv_buffer = &mau32_buffer_data,
    .u16_buffer_size_bytes = sizeof(mau32_buffer_data),
    .u8_item_size_bytes = sizeof(mau32_buffer_data[0]),
};

/**
 * @brief Predefined data used for testing
 *
 * Can be used in tests in order to fill ring buffer with known data
 */
static const uint32_t mau32_example_data[_RING_BUFFER_SIZE_ITEMS] = {
    0xACDC, 0xDEAD, 0xBEEF, 0xCCAA, 0x5003,
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
void setUp(void);
void tearDown(void);
// ==============| Internal function prototypes: middle level |===============
static void _test_push_pull_reset_idx(void);
static void _test_pull_latest(void);
static void _test_pull_relative(void);
static void _test_get_num_of_items(void);
// ================| Internal function prototypes: low level |================
/**
 * @brief Push pre-set data into buffer
 *
 * @param u8_num_of_items Define how many items will be pushed into ring
 * 						  buffer. Data are taken from
 * 						  @ref mau32_example_data
 */
static void _preset_ring_buffer(uint8_t u8_num_of_items);
// =========================| High level functions |==========================
int main(void) {
  UnityBegin(__FILE__);

  RUN_TEST(_test_push_pull_reset_idx);
  RUN_TEST(_test_pull_latest);
  RUN_TEST(_test_pull_relative);
  RUN_TEST(_test_get_num_of_items);

  return (UnityEnd());
}

/**
 * @brief Unity setup function
 *
 * Executed before every test
 *
 */
void setUp(void) {
  // Setup ring buffer
  ring_buffer_clean(&ms_ring_buffer);
}

/**
 * @brief Unity tear down function
 *
 * Executed after every test
 */
void tearDown(void) {}
// ========================| Middle level functions |=========================
static void _test_push_pull_reset_idx(void) {
  // Test case expect buffer size 5. If buffer size is changed, this unit test
  // needs to be updated
  assert(_RING_BUFFER_SIZE_ITEMS == 5);

  // Push 3 values
  _preset_ring_buffer(3);

  // Pull last
  uint32_t u32_tmp = 0;
  // Now pull & check
  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);

  TEST_ASSERT_EQUAL_HEX32(0xACDC, u32_tmp);

  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);

  TEST_ASSERT_EQUAL_HEX32(0xDEAD, u32_tmp);

  // This should get us to the beginning
  ring_buffer_reset_read_idx(&ms_ring_buffer);

  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);

  // Buffer was not overwritten completely, so expected zero (buffer is cleaned
  // before every unit test)
  TEST_ASSERT_EQUAL_HEX32(0, u32_tmp);

  // Buffer size is 5 items. Write 2 more in order to set write index back to
  // very first item in ring buffer
  u32_tmp = 0xCC;
  ring_buffer_push(&ms_ring_buffer, &u32_tmp);
  ring_buffer_push(&ms_ring_buffer, &u32_tmp);

  ring_buffer_reset_read_idx(&ms_ring_buffer);
  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);

  TEST_ASSERT_EQUAL_HEX32(0xACDC, u32_tmp);
}

static void _test_pull_latest(void) {
  // Test case expect buffer size 5. If buffer size is changed, this unit test
  // needs to be updated
  assert(_RING_BUFFER_SIZE_ITEMS == 5);

  // Push 4 values
  _preset_ring_buffer(4);

  uint32_t u32_tmp = 0;
  ring_buffer_pull_latest(&ms_ring_buffer, &u32_tmp);

  // Read index should be still zero
  TEST_ASSERT_EQUAL_HEX32(0, ms_ring_buffer.u16_read_idx);
  // Write index should be 4 then
  TEST_ASSERT_EQUAL_HEX32(4, ms_ring_buffer.u16_write_idx);

  // Read 1st item
  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[0], u32_tmp);

  TEST_ASSERT_EQUAL_HEX32(1, ms_ring_buffer.u16_read_idx);
  TEST_ASSERT_EQUAL_HEX32(4, ms_ring_buffer.u16_write_idx);

  // Push 1 more item -> should increase write index, read index shall be same
  u32_tmp = 0xBEEE;
  ring_buffer_push(&ms_ring_buffer, &u32_tmp);

  TEST_ASSERT_EQUAL_HEX32(0, ms_ring_buffer.u16_write_idx);
  TEST_ASSERT_EQUAL_HEX32(1, ms_ring_buffer.u16_read_idx);

  // Discard content in temporary variable. Read latest value. No index shall be
  // changed
  u32_tmp = 0;
  ring_buffer_pull_latest(&ms_ring_buffer, &u32_tmp);

  TEST_ASSERT_EQUAL_HEX32(0xBEEE, u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(0, ms_ring_buffer.u16_write_idx);
  TEST_ASSERT_EQUAL_HEX32(1, ms_ring_buffer.u16_read_idx);
}

static void _test_pull_relative(void) {
  // Test case expect buffer size 5. If buffer size is changed, this unit test
  // needs to be updated
  assert(_RING_BUFFER_SIZE_ITEMS == 5);

  // Fill buffer to the top and push 2 more items. This will set write index
  // to non zero value (suitable for this unit test)
  _preset_ring_buffer(_RING_BUFFER_SIZE_ITEMS);
  _preset_ring_buffer(2);

  TEST_ASSERT_EQUAL_HEX32_ARRAY(mau32_example_data, mau32_buffer_data,
                                _RING_BUFFER_SIZE_ITEMS);
  TEST_ASSERT_EQUAL_HEX32(0, ms_ring_buffer.u16_read_idx);
  TEST_ASSERT_EQUAL_HEX32(2, ms_ring_buffer.u16_write_idx);

  // Read 4x times in order to move read index ahead
  uint32_t u32_tmp = 0;
  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[0], u32_tmp);

  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[1], u32_tmp);

  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[2], u32_tmp);

  ring_buffer_pull(&ms_ring_buffer, &u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[3], u32_tmp);

  // Check read and write indexes. Read index shall be changed, write index
  // shall be same
  TEST_ASSERT_EQUAL_HEX32(4, ms_ring_buffer.u16_read_idx);
  TEST_ASSERT_EQUAL_HEX32(2, ms_ring_buffer.u16_write_idx);

  // Latest data shall be at write index position (since it is place, when new
  // data will be written and where oldest data persists). Read index shall not
  // be changed
  ring_buffer_pull_relative(&ms_ring_buffer, &u32_tmp, 0);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[2], u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(4, ms_ring_buffer.u16_read_idx);

  ring_buffer_pull_relative(&ms_ring_buffer, &u32_tmp, 1);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[3], u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(4, ms_ring_buffer.u16_read_idx);

  ring_buffer_pull_relative(&ms_ring_buffer, &u32_tmp, 2);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[4], u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(4, ms_ring_buffer.u16_read_idx);

  ring_buffer_pull_relative(&ms_ring_buffer, &u32_tmp, 3);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[0], u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(4, ms_ring_buffer.u16_read_idx);

  ring_buffer_pull_relative(&ms_ring_buffer, &u32_tmp, 4);
  TEST_ASSERT_EQUAL_HEX32(mau32_example_data[1], u32_tmp);
  TEST_ASSERT_EQUAL_HEX32(4, ms_ring_buffer.u16_read_idx);
}

static void _test_get_num_of_items(void) {
  TEST_ASSERT_EQUAL_HEX16(_RING_BUFFER_SIZE_ITEMS,
                          ring_buffer_get_num_of_items(&ms_ring_buffer));
}
// ==========================| Low level functions |==========================
static void _preset_ring_buffer(uint8_t u8_num_of_items) {
  // Can not push more than what is in example array
  assert(u8_num_of_items <= _RING_BUFFER_SIZE_ITEMS);

  for (uint8_t u8_item_idx = 0; u8_item_idx < u8_num_of_items; u8_item_idx++) {
    ring_buffer_push(&ms_ring_buffer, &mau32_example_data[u8_item_idx]);
  }
}
// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
