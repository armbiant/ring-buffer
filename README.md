# About

* Simple ring buffer utility
* Every instance of ring buffer can have different item size -> quite universal
* Target for embedded platforms. Size is limited to 65k items, but possible to extend when change variable types

* This library is platform independent